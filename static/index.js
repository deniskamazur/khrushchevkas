var SPEED = 0.003
var screenTouched = false

try {
    window.ontouchstart = function() {
        screenTouched = true
    }

    window.ontouchcancel = function() {
        screenTouched = false
    }

    window.ontouchend = function() {
        screenTouched = false
    }
    SPEED = 0.002;
} catch(e) {
    console.log("deeep")
}

AFRAME.registerComponent('log', {
  schema: {type: 'string'},
  init: function () {
    var stringToLog = this.data;
    console.log(stringToLog);
  }
});


AFRAME.registerComponent('move-control', {
    init: function () {
        window.addEventListener("keydown", (e) => {
            if (e.code === "KeyR") {
                var angle = camera.getAttribute("rotation")
                var x = 1 * Math.cos(angle.y * Math.PI / 180)
                var y = 1 * Math.sin(angle.y * Math.PI / 180)
                var pos = camera.getAttribute("position")
                pos.x -= y
                pos.z -= x;
                player.setAttribute("position", pos);
            }
        })
    },

    tick: function (time, timeDelta) {
        if (screenTouched) {
            var angle = this.el.object3D.rotation
            
            var x = Math.cos(angle.y)
            var y = Math.sin(angle.y)

            this.el.object3D.position.z -= x * SPEED * timeDelta
            this.el.object3D.position.x -= y * SPEED * timeDelta
        }
    }
})

AFRAME.registerComponent('cursor-listener', {
    schema: {
        message: {type: 'string', default: ''}
    },
    init: function () {
        var data = this.data.message;
        console.log(data + " inited ___________________________________________________")
        var info = document.querySelector("#user_text");
        this.el.addEventListener('click', function (evt) {
            info.setAttribute("text", "value:" + data);
        });
    }
});