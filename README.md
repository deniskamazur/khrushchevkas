# VR Khrushchevkas

This is a small project made as a history hometask for Moscow School #1505.

## What is a Khrushchevka?

Khrushchyovka is an unofficial name of a type of low-cost, concrete-paneled or brick three- to five-storied apartment building which was developed in the Soviet Union during the early 1960s, during the time its namesake Nikita Khrushchev directed the Soviet government. (Text from [this wikipedia article](https://en.wikipedia.org/wiki/Khrushchyovka))

## Technologies used:
  
  1. [A-Frame](https://aframe.io/) for VR
  2. [SketchUp](https://www.sketchup.com/) for modeling the house itself